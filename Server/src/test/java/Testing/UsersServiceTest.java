package Testing;

import BsmchFlights.Models.Reservation;
import BsmchFlights.Models.User;
import BsmchFlights.Repositories.UsersRepository;
import BsmchFlights.Services.UsersService;
import BsmchFlights.mailUtils.MailService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(SpringRunner.class)
public class UsersServiceTest {

  @TestConfiguration
  static class EmployeeServiceImplTestContextConfiguration {
    @Bean
    public UsersService usersService() {
      return new UsersService();
    }
  }

  @Autowired @SpyBean public UsersService usersService;

  @MockBean public UsersRepository usersRepository;
  @MockBean public MailService mailService;

  public User firstUser;
  public User secondUser;
  public User thirdUser;
  public User fourthUser;
  public ArrayList<Reservation> reservations;
  public Reservation firstReservation;
  public Reservation secondReservation;

  @Before
  public void setUp() {
    firstUser = new User("someMail@gamil.com");
    firstReservation = new Reservation(firstUser);
    reservations = new ArrayList<>();
    reservations.add(firstReservation);
  }

  @Test
  public void updateFlight_checkForMailSenderCalledOnce() {
    Mockito.when(usersRepository.findById(Mockito.anyInt()))
        .thenReturn(Optional.ofNullable(firstUser));
    usersService.updateFlightPrice(reservations, 1000);
    Mockito.verify(mailService, Mockito.atLeastOnce())
        .sendEmail(anyString(), anyString(), anyString());
  }

  @Test(expected = Exception.class)
  public void updateFlights_checkForMailSenderNotToCalledThrowsException() {
    Mockito.when(usersRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
    usersService.updateFlightPrice(reservations, 100);
  }

  @Test
  public void updateFlights_checkForCalledTwice() {
    secondUser = new User("someMail@gamil.com");
    secondReservation = new Reservation(secondUser);
    reservations.add(secondReservation);

    Mockito.when(usersRepository.findById(Mockito.anyInt()))
        .thenReturn(Optional.ofNullable(firstUser));
    usersService.updateFlightPrice(reservations, 100);

    Mockito.verify(mailService, Mockito.atLeast(2))
        .sendEmail(anyString(), anyString(), anyString());
  }

  @Test
  public void updateFlights_checkForCalledMultiplyTimes() {
    secondUser = new User();
    secondReservation = new Reservation(secondUser);
    reservations.add(secondReservation);
    thirdUser = new User();
    Reservation thirdReservation = new Reservation(thirdUser);
    reservations.add(thirdReservation);
    fourthUser = new User();
    Reservation fourthReservation = new Reservation(fourthUser);
    reservations.add(fourthReservation);

    Mockito.when(usersRepository.findById(Mockito.anyInt()))
        .thenReturn(Optional.ofNullable(firstUser));
    usersService.updateFlightPrice(reservations, 100);

    Mockito.verify(mailService, Mockito.atLeast(4))
        .sendEmail(anyString(), anyString(), anyString());
  }
}
