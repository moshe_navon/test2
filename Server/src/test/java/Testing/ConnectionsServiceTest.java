package Testing;

import BsmchFlights.Models.Airport;
import BsmchFlights.Models.ConnectionFlight;
import BsmchFlights.Models.Flight;
import BsmchFlights.Services.AirportsService;
import BsmchFlights.Services.ConnectionsService;
import BsmchFlights.Services.FlightsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ConnectionsServiceTest {

  @TestConfiguration
  static class FlightsServiceTestingContextConfiguration {
    @Bean
    public ConnectionsService connectionsService() {
      return new ConnectionsService();
    }
  }

  @Autowired @SpyBean public ConnectionsService connectionsService;
  @MockBean AirportsService airportsService;
  @MockBean FlightsService flightsService;

  private Airport TLV;
  private Airport AVL;
  private Airport DUB;
  private Airport LHR;
  private Flight firstFlight;
  private Flight secondFlight;
  private ArrayList<Flight> flights;
  private ArrayList<ConnectionFlight> connectionFlights;
  private ConnectionFlight connectionFlight;

  @Before
  public void setUp() {
    TLV = new Airport("TLV");
    AVL = new Airport("AVL");
    DUB = new Airport("DUB");
    LHR = new Airport("LHR");
    flights = new ArrayList<>();
    connectionFlights = new ArrayList<>();

    Mockito.when(airportsService.getByIata("DUB")).thenReturn(DUB);
    Mockito.when(airportsService.getByIata("AVL")).thenReturn(AVL);
    Mockito.when(airportsService.getByIata("TLV")).thenReturn(TLV);
    Mockito.when(airportsService.getByIata("LHR")).thenReturn(LHR);
  }

  @Test
  public void isDistanceValid_ConnectionBetweenAirports_Test() {
    Airport rightAirport = new Airport();
    rightAirport.setLat(0);
    rightAirport.setLon(0);

    Airport leftAirport = new Airport();
    leftAirport.setLat(1);
    leftAirport.setLon(1);

    Airport connectionAirport = new Airport();
    connectionAirport.setLat(0.5);
    connectionAirport.setLon(0.5);

    assertThat(connectionsService.isDistanceValid(rightAirport, leftAirport, connectionAirport))
        .isTrue();
  }

  @Test
  public void isDistanceValid_InsideDistanceRadius_Test() {
    Airport rightAirport = new Airport();
    rightAirport.setLat(0);
    rightAirport.setLon(0);

    Airport leftAirport = new Airport();
    leftAirport.setLat(-2);
    leftAirport.setLon(-2);

    Airport connectionAirport = new Airport();
    connectionAirport.setLat(-3);
    connectionAirport.setLon(-3);

    assertThat(connectionsService.isDistanceValid(rightAirport, leftAirport, connectionAirport))
        .isTrue();
  }

  @Test
  public void isDistanceValid_OutsideDistanceRadius_Test() {
    Airport rightAirport = new Airport();
    rightAirport.setLat(0);
    rightAirport.setLon(0);

    Airport leftAirport = new Airport();
    leftAirport.setLat(-1);
    leftAirport.setLon(-1);

    Airport connectionAirport = new Airport();
    connectionAirport.setLat(-2);
    connectionAirport.setLon(-2);

    assertThat(connectionsService.isDistanceValid(rightAirport, leftAirport, connectionAirport))
        .isFalse();
  }

  @Test
  public void isTimeBetweenFlightsValid_noPreviousFlight_Test() {
    Date takeoff = null;

    assertThat(connectionsService.isTimeBetweenFlightsValid(new Date(), takeoff)).isTrue();
  }

  @Test
  public void isTimeBetweenFlightsValid_sameDayValid_Test() throws ParseException {
    Date arrival = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 08:00:00");
    Date takeoff = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 14:00:00");

    assertThat(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff)).isTrue();
  }

  @Test
  public void isTimeBetweenFlightsValid_sameDayNotValid_Test() throws ParseException {
    Date arrival = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 01:00:00");
    Date takeoff = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 23:00:00");

    assertThat(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff)).isFalse();
  }

  @Test
  public void isTimeBetweenFlightsValid_differentDayValid_Test() throws ParseException {
    Date arrival = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 20:00:00");
    Date takeoff = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-02 01:00:00");

    assertThat(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff)).isTrue();
  }

  @Test
  public void isTimeBetweenFlightsValid_differentDayNotValid_Test() throws ParseException {
    Date arrival = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 08:00:00");
    Date takeoff = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-02 14:00:00");

    assertThat(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff)).isFalse();
  }

  @Test
  public void isTimeBetweenFlightsValid_takeoffBeforeArrival_Test() throws ParseException {
    Date arrival = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 14:00:00");
    Date takeoff = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 08:00:00");
    System.out.println(arrival);
    System.out.println(takeoff);
    System.out.println(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff));
    System.out.println(connectionsService.isTimeBetweenFlightsValid(takeoff, arrival));

    assertThat(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff)).isFalse();
  }

  @Test
  public void isTimeBetweenFlightsValid_differenceIsMin_Test() throws ParseException {
    Date arrival = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 08:00:00");
    Date takeoff = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 09:00:00");

    assertThat(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff)).isTrue();
  }

  @Test
  public void isTimeBetweenFlightsValid_differenceIsMax_Test() throws ParseException {
    Date arrival = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-01 08:00:00");
    Date takeoff = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse("2019-01-02 04:00:00");

    assertThat(connectionsService.isTimeBetweenFlightsValid(arrival, takeoff)).isTrue();
  }

  @Test
  public void getFlights_noFlightsToDestination_test() throws ParseException {
    Date on = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01");
    assertThat(connectionsService.getFlights("DUB", "A", "A", on, 0, null))
        .isEqualTo(new ArrayList<>());
  }

  @Test
  public void getFlights_noFlightsFromOrigin_test() throws ParseException {
    Date on = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01");
    assertThat(connectionsService.getFlights("A", "AVL", "AVL", on, 1, null))
        .isEqualTo(new ArrayList<>());
  }

  @Test
  public void getFlights_oneFlightBetweenWithNoConnection() throws ParseException {
    Date on = new SimpleDateFormat("yyyy-MM-dd").parse("2021-03-21");
    firstFlight =
        new Flight(10, AVL, DUB, new Date(on.getTime()), new Date(on.getTime() + 8460000));
    flights.add(firstFlight);
    connectionFlight = new ConnectionFlight(AVL, DUB, firstFlight);
    connectionFlights.add(connectionFlight);
    Mockito.when(flightsService.getFlightsFromToAt("AVL", "DUB", on)).thenReturn(flights);

    assertThat(connectionsService.getFlights("AVL", "DUB", "AVL", on, 0, null))
        .isEqualTo(connectionFlights);
  }

//  @Test
//  public void getFlights_oneFlightBetweenWithConnection() throws ParseException {
      // TODO create test for connections
//  }
}
