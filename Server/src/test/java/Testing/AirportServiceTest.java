package Testing;

import BsmchFlights.Models.Airport;
import BsmchFlights.Repositories.AirportsRepository;
import BsmchFlights.Services.AirportsService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class AirportServiceTest {

  @TestConfiguration
  static class AirportServiceImplTestContextConfiguration {
    @Bean
    public AirportsService airportsService() {
      return new AirportsService();
    }
  }

  @Autowired AirportsService airportsService;
  @MockBean AirportsRepository airportsRepository;

  public ArrayList<Airport> airports;
  public Airport firstAirport;
  public Airport secondAirport;
  public Airport thirdAirport;
  public Airport fourthAirport;
  public double[] boundsArray;

  @Before
  public void setUp() {
    firstAirport = new Airport();
    airports = new ArrayList<>();
    airports.add(firstAirport);
    boundsArray = new double[4];
    boundsArray[0] = 0;
    boundsArray[1] = 1;
    boundsArray[2] = 2;
    boundsArray[3] = 3;
  }

  @Test
  public void getAirportsByBounds_getOneAirport() {
    when(airportsRepository.findByLatBetweenAndLonBetween(
            boundsArray[0], boundsArray[2], boundsArray[1], boundsArray[3]))
        .thenReturn(airports);
    List<Airport> expected = airportsService.getAirportsByBounds(boundsArray);
    assertEquals(expected, airports);
  }

  @Test
  public void getAirportByBound_checkForCallingToRepository() {
    when(airportsRepository.findByLatBetweenAndLonBetween(
            boundsArray[0], boundsArray[2], boundsArray[1], boundsArray[3]))
        .thenReturn(airports);
    airportsService.getAirportsByBounds(boundsArray);
    verify(airportsRepository, atLeastOnce()).findByLatBetweenAndLonBetween(0, 2, 1, 3);
  }
}
