package BsmchFlights.Services;

import BsmchFlights.Models.Airport;
import BsmchFlights.Models.ConnectionFlight;
import BsmchFlights.Models.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ConnectionsService {

  private final Logger logger = LoggerFactory.getLogger(ConnectionsService.class);

  @Autowired private AirportsService airportsService;
  @Autowired private FlightsService flightsService;

  public ConnectionsService() {}

  public List<ConnectionFlight> getFlights(
      String from, String to, String originalTo, Date at, int stops, Date nextTakeOffTime) {

    List<ConnectionFlight> flights = new ArrayList<>();
    Airport fromAirport = this.airportsService.getByIata(from);
    Airport toAirport = this.airportsService.getByIata(to);

    try {
      logger.info("Trying to get flights");
      if (stops == 0) {
        this.flightsService
            .getFlightsFromToAt(from, to, at)
            .forEach(flight -> flights.add(new ConnectionFlight(fromAirport, toAirport, flight)));
      } else {
        List<Flight> flightsOnDates = this.flightsService.getFlightsToAt(to, at, true);

        for (Flight flight : flightsOnDates) {
          if (isTimeBetweenFlightsValid(flight.arrivalTime(), nextTakeOffTime)) {
            if ((flight.arrivalAirport().iata()).equals(from)) {
              flights.add(new ConnectionFlight(fromAirport, toAirport, flight));
            } else {
              if (isDistanceValid(fromAirport, toAirport, flight.takeoffAirport())) {
                flights.addAll(
                    findConnectionsWithFlight(
                        flight,
                        from,
                        flight.takeoffAirport().iata(),
                        originalTo,
                        at,
                        --stops,
                        flight.takeoffTime()));
              }
            }
          }
        }
      }
      logger.info("Success to get " + flights.size() + " flights");

    } catch (Exception exception) {
      logger.error("Unable to get flights, Error: " + exception.getMessage());
    }

    return flights;
  }

  public List<ConnectionFlight> findConnectionsWithFlight(
      Flight flight,
      String from,
      String to,
      String originalTo,
      Date at,
      int stops,
      Date nextTakeOffTime) {

    logger.info("Trying to get possible connections flights");
    List<ConnectionFlight> flights =
        this.getFlights(from, to, originalTo, at, stops, nextTakeOffTime);

    flights.forEach(
        innerFlight -> {
          Flight currentFlight = innerFlight.flights().get(innerFlight.flights().size() - 1);

          if (isTimeBetweenFlightsValid(currentFlight.arrivalTime(), nextTakeOffTime)) {
            innerFlight.addFlight(flight);
          } else {
            flights.remove(innerFlight);
          }
        });
    logger.info("Success to get " + flights.size() + " flights");

    return flights;
  }

  public boolean isTimeBetweenFlightsValid(Date arrival, Date takeoff) {
    logger.info("Check is time between flights valid");
    boolean isValid = true;
    final int minWaitingInMIN = 60;
    final int maxWaitingInMIN = 1200;

    if (takeoff != null) {
      long diffInMSC = takeoff.getTime() - arrival.getTime();
      long diffInMIN = TimeUnit.MILLISECONDS.toMinutes(diffInMSC);

      isValid = (diffInMIN >= minWaitingInMIN && diffInMIN <= maxWaitingInMIN);
    }

    return isValid;
  }

  public boolean isDistanceValid(Airport origin, Airport dest, Airport connect) {
    logger.info("Check is distance between airports valid");

    double maxDist = this.distance(origin.lat(), origin.lon(), dest.lat(), dest.lon()) * 0.75;

    return ((this.distance(origin.lat(), origin.lon(), connect.lat(), connect.lon()) <= maxDist)
        || (this.distance(dest.lat(), dest.lon(), connect.lat(), connect.lon()) <= maxDist));
  }

  public double distance(double lat1, double lon1, double lat2, double lon2) {
    logger.info("Calc distance between airports");
    double dist = 0;

    if ((lat1 != lat2) || (lon1 != lon2)) {
      double theta = lon1 - lon2;
      dist =
          Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2))
              + Math.cos(Math.toRadians(lat1))
                  * Math.cos(Math.toRadians(lat2))
                  * Math.cos(Math.toRadians(theta));
      dist = Math.toDegrees(Math.acos(dist));
      dist *= 60 * 1.1515;
    }

    return dist;
  }
}
