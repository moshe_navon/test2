package BsmchFlights.Services;

import BsmchFlights.Models.ConnectionFlight;
import BsmchFlights.Models.Flight;
import BsmchFlights.Repositories.FlightsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class FlightsService {

    private final Logger logger = LoggerFactory.getLogger(FlightsService.class);
    private final int oneDayInMSC = 86400000;

    @Autowired
    private FlightsRepository flightsRepository;
    @Autowired
    private ReservationsService reservationsService;

    public List<Flight> getAll() {
        List<Flight> flights = new ArrayList<>();

        try {
            logger.info("Trying to get all flights");
            flights = this.flightsRepository.findAll();
            logger.info("Success to get " + flights.size() + " flights");

        } catch (Exception exception) {
            logger.error("Unable to get all airports, Error: " + exception.getMessage());
        }

        return flights;
    }

    public List<Flight> getFiveFirstFlightsFromIata(String IATA, Date at) {
        List<Flight> flights = new ArrayList<>();

        try {
            logger.info("Trying to get first 5 flights");
            flights = this.getFlightsFromAt(IATA, at).stream().limit(5).collect(Collectors.toList());
            logger.info("Success to get " + flights.size() + " flights");

        } catch (Exception exception) {
            logger.error("Unable to get first 5 flights, Error: " + exception.getMessage());
        }

        return flights;
    }

    public List<Flight> getFlightsToCityName(String name) {
        List<Flight> flights = new ArrayList<>();

        try {
            logger.info("Trying to get flights by city name");
            flights = this.flightsRepository.findByArrivalAirport_city_name(name);
            logger.info("Success to get " + flights.size() + " flights");

        } catch (Exception exception) {
            logger.error("Unable get flights by city name, Error: " + exception);
        }

        return flights;
    }

    public List<Flight> getFlightsFromTo(String from, String to) {
        List<Flight> flights = new ArrayList<>();

        try {
            logger.info("Try to get flights by from and to city");
            flights = this.flightsRepository.findByTakeoffAirport_IataAndArrivalAirport_Iata(from, to);
            logger.info("Success to get " + flights.size() + " flights");

        } catch (Exception exception) {
            logger.error("Unable get lights from and to city, Error: " + exception);
        }

        return flights;
    }

    public List<Flight> getFlightsFromToAt(String from, String to, Date at) {
        List<Flight> flights = new ArrayList<>();

        Date after = new Date(at.getTime() + oneDayInMSC);

        try {
            logger.info("Trying to get flights by from and to at time");
            flights =
                    this.filterSeats(
                            this.flightsRepository
                                    .findByTakeoffAirport_IataAndArrivalAirport_IataAndTakeofftimeBetween(
                                            from, to, at, after));
            logger.info("Success to get " + flights.size() + " flights");

        } catch (Exception exception) {
            logger.error("Unable get flights by from and to at time, Error: " + exception);
        }

        return flights;
    }

    public List<Flight> getFlightsFromAt(String from, Date at) {
        List<Flight> flights = new ArrayList<>();
        Date after = new Date(at.getTime() + oneDayInMSC);

        try {
            logger.info("Trying to get flights by from and at time");
            flights =
                    this.filterSeats(
                            this.flightsRepository.findByTakeoffAirport_IataAndTakeofftimeBetween(
                                    from, at, after));
            logger.info("Success to get " + flights.size() + " flights");

        } catch (Exception exception) {
            logger.error("Unable get flights by from and at time, Error: " + exception);
        }

        return flights;
    }

    public List<Flight> getFlightsToAt(String to, Date at, boolean isConnection) {
        List<Flight> flights = new ArrayList<>();
        Date after = new Date(at.getTime() + (oneDayInMSC * ((isConnection) ? 2 : 1)));

        try {
            logger.info("Trying get flights by to and at time");
            flights =
                    this.filterSeats(
                            this.flightsRepository.findByArrivalAirport_IataAndTakeofftimeBetween(to, at, after));
            logger.info("Success to get " + flights.size() + " flights");

        } catch (Exception exception) {
            logger.error("Unable get flights by to and at time, Error: " + exception);
        }

        return flights;
    }

    public List<Flight> filterSeats(List<Flight> flights) {

        Iterator itr = flights.iterator();

        while (itr.hasNext()) {
            Flight flight = (Flight) itr.next();
            long numberOfSeatsTaken =
                    this.reservationsService.getNumberOfSeatsTakenForFlight(flight.id());
            if (flight.plane().seatsQuantity() == numberOfSeatsTaken) {
                itr.remove();
            }
        }

        return flights;
    }

    public List<ConnectionFlight> formatFlights(List<Flight> flights) {
        logger.info("Formatting flights into connect flights");
        List<ConnectionFlight> formattedFlights = new ArrayList<>();

        flights.forEach(
                flight ->
                        formattedFlights.add(
                                new ConnectionFlight(flight.takeoffAirport(), flight.arrivalAirport(), flight)));

        return formattedFlights;
    }

    public void updateFlight(Flight flight) {
        flightsRepository.save(flight);
        this.reservationsService.updateFlightPrice(flight.id(), flight.price());
    }
	
	public List<Flight> getTopNewFlights() {
    return this.flightsRepository.findTop5ByOrderByTakeofftimeDesc();
  }
}
