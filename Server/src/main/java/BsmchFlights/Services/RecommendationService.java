package BsmchFlights.Services;

import BsmchFlights.Models.Recommendation;
import BsmchFlights.Repositories.RecommendationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecommendationService {

    private final Logger logger = LoggerFactory.getLogger(RecommendationService.class);

    @Autowired
    private RecommendationRepository recommendationRepository;

    public List<Recommendation> getRecommendations() {
        return recommendationRepository.findAll();
    }

    public void addRecommendation(Recommendation recommendation) {
        recommendationRepository.save(recommendation);
    }
}
