package BsmchFlights.Services;

import BsmchFlights.Models.Plane;
import BsmchFlights.Repositories.PlanesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlanesService {

  private final Logger logger = LoggerFactory.getLogger(CitiesService.class);

  @Autowired private PlanesRepository planesRepository;

  public List<Plane> getAll() {
    List<Plane> planes = new ArrayList<>();

    try {
      logger.info("Trying to get all planes");
      planes = this.planesRepository.findAll();
      logger.info("Success to get " + planes.size() + " planes");

    } catch (Exception exception) {
      logger.error("Unable to get all planes, Error: " + exception.getMessage());
    }

    return planes;
  }
}
