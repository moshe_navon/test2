package BsmchFlights.Services;

import BsmchFlights.Models.City;
import BsmchFlights.Repositories.CitiesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class CitiesService {

  private final Logger logger = LoggerFactory.getLogger(CitiesService.class);

  @Autowired private CitiesRepository citiesRepository;

  public List<City> getAll() {
    List<City> cities = new ArrayList<>();

    try {
      logger.info("Trying to get all cities");
      cities = this.citiesRepository.findAll();
      logger.info("Success to get " + cities.size() + " cities");

    } catch (Exception exception) {
      logger.error("Unable to get all cities: Error: " + exception.getMessage());
    }

    return cities;
  }
}
