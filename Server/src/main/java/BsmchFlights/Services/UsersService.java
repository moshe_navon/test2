package BsmchFlights.Services;

import BsmchFlights.Config.JwtProperties;
import BsmchFlights.Exceptions.UserNotFoundException;
import BsmchFlights.Models.Reservation;
import BsmchFlights.Models.User;
import BsmchFlights.Repositories.UsersRepository;
import BsmchFlights.mailUtils.MailService;
import com.auth0.jwt.JWT;
import net.bytebuddy.utility.RandomString;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

@Service
public class UsersService {

    private final Logger logger = LoggerFactory.getLogger(UsersService.class);

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private MailService mailService;

    public List<User> getAll() {
        List<User> users = new ArrayList<>();

        try {
            logger.info("Trying to get all users from DB");
            users = this.usersRepository.findAll();
            logger.info("Success to get " + users.size() + " users");

        } catch (Exception err) {
            logger.error("Can't find users in DB");
        }
        return users;
    }

    public boolean checkIfUserExistsByEmail(String email) {
        logger.info("Seeing If Anyone Has A email");
        return usersRepository.findByEmail(email) != null;
    }

    public void sendChangePasswordEmail(String email, HttpServletRequest request) throws UserNotFoundException {
        logger.info("changing password for email: " + email);
        String token = RandomString.make(45);
        String url = request.getRequestURL().toString();
        url = getOriginalUrl(url) + "/users/resetPassword/" + token;
        changePasswordByEmail(email, token);
        sendEmail(email, url);
        logger.info("sent email");
    }

    public String insertUser(User user) {
        String token = null;

        try {
            logger.info("Trying to add new user to DB");
            User userRes = this.usersRepository.save(user);
            logger.info("Success to add new user");

            logger.info("Trying to create json of user");
            JSONObject userJson = new JSONObject();
            userJson.put("image", userRes.image());
            userJson.put("creditCard", userRes.creditCard());
            userJson.put("username", userRes.username());
            userJson.put("permission", userRes.roles());
            userJson.put("id", user.id());
            logger.info("Success to create json of user");

            logger.info("Trying to create token");
            token =
                    JWT.create()
                            .withClaim("user", String.valueOf(userJson))
                            .withSubject(userRes.username())
                            .withExpiresAt(new Date(System.currentTimeMillis() + JwtProperties.EXPIRATION_TIME))
                            .sign(HMAC512(JwtProperties.SECRET.getBytes()));
            logger.info("Success to create token " + token);

        } catch (Exception exception) {
            logger.error("Can't add new user, Error: " + exception.getMessage());
        }

        return token;
    }

    public User getUserByEmail(String email) {
        return usersRepository.findByEmail(email);
    }

    public void changePassword(String email) {
        User user = usersRepository.findByEmail(email);
        String subject = "Forgot password";
        String text = " The old password is: " + user.password();
        this.mailService.sendEmail(email, subject, text);
    }

    public void notifyFlightPrice(ArrayList<Reservation> reservations, double newPrice) {
        String subject = "change of price in your flight";
        String text = "the new price is " + newPrice;
        for (Reservation reservation : reservations) {
            Optional<User> users = this.usersRepository.findById(reservation.userId().id());
            this.mailService.sendEmail(users.get().email(), subject, text);
        }
    }

    public User getByPasswordToken(String token) {
        logger.info("getting user: ");
        return this.usersRepository.findByResetPasswordToken(token);
    }

    public void updatePassword(User user, String newPassword) {
        logger.info("updating password");
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);

        user.updatePasswordToken(null);

        usersRepository.save(user);
    }

    public void updateFlightPrice(ArrayList<Reservation> reservations, double newPrice) {
        String subject = "change of price in your flight";
        String text = "the new price is " + newPrice;

        for (Reservation reservation : reservations) {
            Optional<User> users = this.usersRepository.findById(reservation.userId().id());
            this.mailService.sendEmail(users.get().email(), subject, text);
        }

    }

    public void changePasswordByEmail(String email, String token) throws UserNotFoundException {
        logger.info("changing password by email :" + email);
        User user = usersRepository.findByEmail(email);

        if (user != null) {
            user.updatePasswordToken(token);
            usersRepository.save(user);
        } else {
            throw new UserNotFoundException("user with this email not found: " + email);
        }
    }


    public void sendEmail(String email, String url) {
        final String subject = "resetting password";
        final String text = "Hello \n" +
                "You have requested to change your password \n" +
                "to reset your password please go to the link below \n" +
                url;
        this.mailService.sendEmail(email, subject, text);
    }

    public String getOriginalUrl(String url) {
        int indexOfFirstSlash = url.indexOf('/', 3);
        //takes the original url and cuts out the first 2 slashes
        String garbage = url.substring(indexOfFirstSlash + 2);
        //finds the next slash index and adds it to the old one + the next slash
        int real = garbage.indexOf('/') + indexOfFirstSlash + 2;

        return url.substring(0, real);
    }

    public boolean changePasswordWithToken(String token, String password) throws UserNotFoundException {
        User user = getByPasswordToken(token);

        if (user == null) {
            throw new UserNotFoundException("Sorry there was no user");
        } else {
            updatePassword(user, password);
            return true;
        }
    }

    public ResponseEntity register(User user, HttpServletRequest req) {
        user.setPassword(passwordEncoder().encode(user.password()));

        String token = RandomString.make(45);
        user.updateEnableToken(token);

        String url = req.getRequestURL().toString();
        url = getOriginalUrl(url) + "/users/verifyEmail/" + token;
        String subject = "verify your email account";
        String text = "to verify your email account please go to the link below \n" +
                url;

        mailService.sendEmail(user.email(), subject, text);
        insertUser(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    public String returnChangeForm(String token) throws UserNotFoundException {
        logger.info("trying to return change password form");
        User user = getByPasswordToken(token);

        if (user == null) {
            throw new UserNotFoundException("user not found");
        }

        return "changePassword.html";
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public String verifyEmail(String token) throws UserNotFoundException {
        logger.info("trying to return verify Email form");
        User user = getByVerifyEmailToken(token);

        if (user == null) {
            throw new UserNotFoundException("user not found");
        }

        user.updateEnableToken(null);
        user.enable();

        usersRepository.save(user);

        return "verifyEmail.html";
    }

    private User getByVerifyEmailToken(String token) {
        return usersRepository.findByEnableToken(token);
    }
}
