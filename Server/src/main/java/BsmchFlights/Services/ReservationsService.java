package BsmchFlights.Services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import BsmchFlights.Models.Reservation;
import BsmchFlights.Repositories.ReservationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationsService {

  @Autowired private ReservationsRepository reservationsRepository;
  @Autowired private UsersService usersService;
  private final Logger logger = LoggerFactory.getLogger(ReservationsService.class);

  public List<Reservation> getAll() {
    List<Reservation> reservations = new ArrayList<>();

    try {
      logger.info("Trying to get all reservations from DB");
      reservations = this.reservationsRepository.findAll();
      logger.info("Success to get " + reservations.size() + " reservations");

    } catch (Exception exception) {
      logger.error("Can't find reservations in DB");
    }

    return reservations;
  }

  public void add(Reservation newRes) {
    try {
      logger.info("Trying to add new reservation to DB");
      this.reservationsRepository.save(newRes);
      logger.info("Success to add new reservations");

    } catch (Exception exception) {
      logger.error("Can't add new reservation");
    }
  }

  public void addList(List<Reservation> newRes) {
    try {
      logger.info("Trying to add list of reservations to DB");

      for (Reservation currRes : newRes) {
        this.reservationsRepository.save(currRes);
      }
      logger.info("Success to add " + newRes.size() + " reservations");

    } catch (Exception exception) {
      logger.error("Can't add list of reservations");
    }
  }

  public Reservation findById(int id) {
    Reservation reservation = new Reservation();

    try {
      logger.info("Trying to get reservation by ID from DB");
      reservation = this.reservationsRepository.findById(id).get();
      logger.info("Success to get reservation");

    } catch (Exception exception) {
      logger.error("Can't find reservation in DB");
    }

    return reservation;
  }

  public List<Reservation> findByConfirmationCode(int confirmationCode) {
    List<Reservation> reservations = new ArrayList<>();

    try {
      logger.info("Trying to get reservations by confirmation Code from DB");
      reservations = this.reservationsRepository.findByOrderConfirmation(confirmationCode);
      logger.info("Success to get " + reservations.size() + " reservations");

    } catch (Exception exception) {
      logger.error("Can't find reservations in DB");
    }

    return reservations;
  }

  public long getNumberOfSeatsTakenForFlight(int flightId) {
    long seatsTaken = 0;

    try {
      seatsTaken = this.reservationsRepository.countByFlightId_idAndIsSeatTaken(flightId, true);

    } catch (Exception exception) {
      logger.error("Can't find seats taken for flight in DB");
    }

    return seatsTaken;
  }

  public List<Reservation> getByUserId(int userId) {
    List<Reservation> reservations = new ArrayList<>();

    try {
      logger.info("Trying to get reservations by user ID from DB");
      reservations = this.reservationsRepository.findByUserId_id(userId);
      logger.info("Success to get " + reservations.size() +" reservations");

    } catch (Exception exception) {
      logger.error("Can't find reservations in DB");
    }

    return reservations;
  }

  public void deleteById(int id){
    try {
      logger.info("Trying to delete reservations by ID from DB");
    this.reservationsRepository.deleteById(id);
      logger.info("Success to deletereservations by ID from DB");
    } catch (Exception exception) {
      logger.error("Can't find reservations in DB");
    }
  }

    public void updateFlightPrice(int id, double newPrice) {
      ArrayList<Reservation> reservations = this.reservationsRepository.findByFlightId_id(id);
      this.usersService.notifyFlightPrice(reservations, newPrice);
    }
}
