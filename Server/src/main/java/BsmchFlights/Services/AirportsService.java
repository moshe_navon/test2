package BsmchFlights.Services;

import BsmchFlights.DTO.AirportDTO;
import BsmchFlights.Models.Airport;
import BsmchFlights.Repositories.AirportsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class AirportsService {

  private final Logger logger = LoggerFactory.getLogger(AirportsService.class);

  @Autowired private AirportsRepository airportsRepository;

  public List<Airport> getAll() {
    List<Airport> airports = new ArrayList<>();

    try {
      logger.info("Trying to get all airports");
      airports = this.airportsRepository.findAll();
      airports.sort(Comparator.comparing(Airport::iata));
      logger.info("Success to get " + airports.size() + " airports");

    } catch (Exception exception) {
      logger.error("Unable to get all airports, Error: " + exception.getMessage());
    }

    return airports;
  }

  public List<AirportDTO> getAirportsReduced() {
    logger.info("Trying to get all airports in DTO Format");
    List<AirportDTO> airportDTOS = new ArrayList<>();

    this.getAll().forEach(airport -> airportDTOS.add(new AirportDTO(airport)));
    logger.info("Success to get " + airportDTOS.size() + " airport DTOs:");

    return airportDTOS;
  }

  public Airport getByIata(String iata) {
    Airport airport = new Airport();

    try {
      logger.info("Trying to get by iata");
      airport = this.airportsRepository.findByIata(iata);
      logger.info("Success to get airport");

    } catch (Exception exception) {
      logger.error("Unable to get by iata, Error: " + exception.getMessage());
    }

    return airport;
  }

  public List<Airport> getAirportsByBounds(double[] bounds) {
    List<Airport> airports = new ArrayList<>();

    try {
      logger.info("Trying to get all airports by bounds");
      logger.info("bounds: " + bounds[1] + ", " + bounds[0] + ", " + bounds[3] + "," + bounds[2]);
      airports =
              this.airportsRepository.findByLatBetweenAndLonBetween(
                      bounds[0], bounds[2], bounds[1], bounds[3]);
      logger.info("Success to get by bounds, " + airports.size() + " airports");

    } catch (Exception exception) {
      logger.error("Unable to get by bounds, Error: " + exception.getMessage());
    }

    return airports;
  }
}
