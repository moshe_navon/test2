package BsmchFlights.Config;

import BsmchFlights.Models.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserPrincipal implements UserDetails {
    private User user;

    public UserPrincipal(User user){
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" +  this.user.roles());
        authorities.add(authority);

        return authorities;
    }

    @Override
    public String getPassword() {
        return this.user.password();
    }

    @Override
    public String getUsername() {
        return this.user.username();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.enabled();
    }

    public String role() {
        return user.roles();
    }

    public long id() {
        return user.id();
    }

    public String creditCard() {
        return user.creditCard();
    }

    public String image() {
        return user.image();
    }

}