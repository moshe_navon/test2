package BsmchFlights.DTO;

import BsmchFlights.Models.Airport;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AirportDTO {

  @JsonProperty
  private String iata;

  @JsonProperty
  private String name;

  public AirportDTO(String iata, String name) {
    this.iata = iata;
    this.name = name;
  }

  public AirportDTO(Airport airport) {
    this.iata = airport.iata();
    this.name = airport.name();
  }

  public String name() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
