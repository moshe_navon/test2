package BsmchFlights.Utils;

public class QueryCondition {

  private String key;
  private String conditionRelation;
  private double[] parameters;

  public QueryCondition(String key, String conditionRelation, double[] parameters) {
    this.key = key;
    this.conditionRelation = conditionRelation;
    this.parameters = parameters;
  }

  public QueryCondition(double[] parameters) {
    this.parameters = parameters;
  }

  public double[] parameters() {
    return parameters;
  }
}
