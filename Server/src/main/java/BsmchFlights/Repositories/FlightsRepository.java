package BsmchFlights.Repositories;

import BsmchFlights.Models.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FlightsRepository extends JpaRepository<Flight, Integer> {
  List<Flight> findByTakeoffAirport_IataAndArrivalAirport_Iata(
      String takeOffLocation_Iata, String arrivalLocation_Iata);

  List<Flight> findByArrivalAirport_city_name(String takeOffCity);

  List<Flight> findByTakeoffAirport_IataAndArrivalAirport_IataAndTakeofftimeBetween(
      String takeOffLocation_Iata, String arrivalLocation_Iata, Date before, Date end);

  List<Flight> findByTakeoffAirport_IataAndTakeofftimeBetween(
      String takeOffLocation_Iata, Date before, Date end);

  List<Flight> findByArrivalAirport_IataAndTakeofftimeBetween(
      String arrivalLocation_Iata, Date before, Date end);

  List<Flight> findTop5ByOrderByTakeofftimeDesc();
}
