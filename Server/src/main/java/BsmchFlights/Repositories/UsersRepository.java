package BsmchFlights.Repositories;

import BsmchFlights.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;


@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {

    UserDetails findUserDetailsByUsername(String username);

    User findByUsername(String username);

    User findByEmail(String email);

    User findByResetPasswordToken(String token);

    User findByEnableToken(String token);

}
