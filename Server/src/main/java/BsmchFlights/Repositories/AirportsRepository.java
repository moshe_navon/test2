package BsmchFlights.Repositories;

import BsmchFlights.Models.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportsRepository extends JpaRepository<Airport, String> {
  Airport findByIata(String iata);

  List<Airport> findByLatBetweenAndLonBetween(double latB, double latA, double lonB, double lonA);
}
