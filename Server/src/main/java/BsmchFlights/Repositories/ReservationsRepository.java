package BsmchFlights.Repositories;

import BsmchFlights.Models.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface ReservationsRepository extends JpaRepository<Reservation, Integer> {

  List<Reservation> findByOrderConfirmation(int confirmationCode);

  long countByFlightId_idAndIsSeatTaken(int flightId, boolean isSeatTaken);

  List<Reservation> findByUserId_id(int userId);

  ArrayList<Reservation> findByFlightId_id(int id);
}
