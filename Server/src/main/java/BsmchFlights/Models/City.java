package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_cities")
public class City {

  @Id
  @JsonProperty
  @Column(name = "id")
  private int id;

  @JsonProperty
  @Column(name = "name")
  private String name;

  public City() {}

  public City(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public String name() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
