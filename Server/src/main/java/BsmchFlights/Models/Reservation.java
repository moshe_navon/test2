package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_reservations")
public class Reservation {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty
  @Column(name = "reservation_id")
  private int reservationId;

  @ManyToOne
  @JsonProperty
  @JoinColumn(name = "user_id")
  private User userId;

  @ManyToOne
  @JsonProperty
  @JoinColumn(name = "flight_id")
  private Flight flightId;

  @JsonProperty
  @Column(name = "passport_id")
  private int passportId;

  @JsonProperty
  @Column(name = "order_confirmation")
  private String orderConfirmation;

  @JsonProperty
  @Column(name = "order_payment")
  private int payment;

  @JsonProperty
  @Column(name = "ticket_code")
  private String ticketCode;

  @JsonProperty
  @Column(name = "passenger_name")
  private String passengerName;

  @JsonProperty
  @Column(name = "passenger_sex")
  private String passengerSex;

  @JsonProperty
  @Column(name = "passenger_birth_date")
  private Date passengerBirthDate;

  @JsonProperty
  @Column(name = "is_seat_taken")
  private boolean isSeatTaken;

  public Reservation() {}

  public Reservation(
      int reservationId,
      User userId,
      Flight flightId,
      String orderConfirmation,
      int payment,
      String ticketCode,
      String passengerName,
      String passengerSex,
      Date passengerBirthDate,
      boolean isSeatTaken) {
    this.reservationId = reservationId;
    this.userId = userId;
    this.flightId = flightId;
    this.orderConfirmation = orderConfirmation;
    this.payment = payment;
    this.ticketCode = ticketCode;
    this.passengerName = passengerName;
    this.passengerBirthDate = passengerBirthDate;
    this.passengerSex = passengerSex;
    this.isSeatTaken = isSeatTaken;
  }

  public Reservation(User userId) {
    this.userId = userId;
  }

  public User userId() {
    return this.userId;
  }
}
