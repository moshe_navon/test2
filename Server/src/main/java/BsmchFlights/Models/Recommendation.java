package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "t_recommendations")
public class Recommendation {

    @Id
    @JsonProperty
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonProperty
    @Column(name = "username")
    private String username;

    @JsonProperty
    @Column(name = "img")
    private String img;

    @JsonProperty
    @Column(name = "rating")
    private double rating;

    @JsonProperty
    @Column(name = "text")
    private String text;
    
}
