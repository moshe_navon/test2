package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "t_airports")
public class Airport {

  @Id
  @JsonProperty
  @Column(name = "iata")
  private String iata;

  @JsonProperty
  @Column(name = "lat")
  private double lat;

  @JsonProperty
  @Column(name = "lon")
  private double lon;

  @JsonProperty
  @Column(name = "name")
  private String name;

  @ManyToOne
  @JsonProperty
  @JoinColumn(name = "city_id")
  private City city;

  public Airport(String iata) {
    this.iata = iata;
  }

  public Airport(String iata, String name) {
    this.iata = iata;
    this.name = name;
  }

  public Airport(String iata, double lat, double lon, String name, City city) {
    this.iata = iata;
    this.lat = lat;
    this.lon = lon;
    this.name = name;
    this.city = city;
  }

  public Airport() {}

  public String iata() {
    return iata;
  }

  public double lat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double lon() {
    return lon;
  }

  public void setLon(double lon) {
    this.lon = lon;
  }

  public String name() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object ob) {
    return this.iata.equals(((Airport) ob).iata());
  }
}
