package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_flights_users")
public class User {

    @Id
    @JsonProperty
    @Column(name = "id")
    private int id;

    @JsonProperty
    @Column(name = "username")
    private String username;

    @JsonProperty
    @Column(name = "firstname")
    private String firstname;

    @JsonProperty
    @Column(name = "lastname")
    private String lastname;

    @JsonProperty
    @Column(name = "email")
    private String email;

    @JsonProperty
    @Column(name = "password")
    private String password;

    @JsonProperty
    @Column(name = "image")
    private String image;

    @JsonProperty
    @Column(name = "joindate")
    private String joindate;

    @JsonProperty
    @Column(name = "creditcard")
    private String creditcard;

    @JsonProperty
    @Column(name = "roles")
    private String roles = "USER";

    @JsonProperty
    @Column(name = "enabled")
    private boolean enabled;

    @JsonProperty
    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    @JsonProperty
    @Column(name = "enabled_token")
    private String enableToken;

    public User() {

    }


    public void enable() {
        this.enabled = true;
    }

    public void updateEnableToken(String enableToken) {
        this.enableToken = enableToken;
    }

    public User(int id, String username, String firstname, String lastname, String email,
                String password, String join_date, String image, String creditcard, String roles) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.joindate = join_date;
        this.image = image;
        this.creditcard = creditcard;
        this.roles = roles;
    }

    public int id() {
        return id;
    }

    public String username() {
        return username;
    }

    public String email() {
        return email;
    }

    public String password() {
        return password;
    }

    public String image() {
        return image;
    }

    public String creditCard() {
        return creditcard;
    }

    public String roles() {
        return this.roles;
    }

    public void updatePasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String email) {
        this.email = email;
    }

    public boolean enabled() {
        return this.enabled;
    }
}
