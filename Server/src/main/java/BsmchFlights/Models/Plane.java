package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_planes")
public class Plane {

  @Id
  @JsonProperty
  @Column(name = "id")
  private int id;

  @JsonProperty
  @Column(name = "planetype")
  private String plane_type;

  @JsonProperty
  @Column(name = "seatsquantity")
  private int seats_quantity;

  public Plane() {}

  public Plane(int id, String plane_type, int seats_quantity) {
    this.id = id;
    this.plane_type = plane_type;
    this.seats_quantity = seats_quantity;
  }

  public int seatsQuantity() {
    return seats_quantity;
  }
}
