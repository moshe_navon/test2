package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ConnectionFlight {

  @JsonProperty
  private final Airport origin;

  @JsonProperty
  private final Airport destination;

  @JsonProperty
  private final List<Flight> flights;

  public ConnectionFlight(Airport origin, Airport destination) {
    this.origin = origin;
    this.destination = destination;
    this.flights = new ArrayList<>();
  }

  public ConnectionFlight(Airport origin, Airport destination, List<Flight> flights) {
    this.origin = origin;
    this.destination = destination;
    this.flights = flights;
  }

  public ConnectionFlight(Airport origin, Airport destination, Flight flight) {
    this(origin, destination);
    this.addFlight(flight);
  }

  public Airport origin() {
    return origin;
  }

  public Airport destination() {
    return destination;
  }

  public List<Flight> flights() {
    return flights;
  }

  public void addFlight(Flight flight) {
    this.flights.add(flight);
  }

  @Override
  public boolean equals(Object ob) {
    ConnectionFlight conFlight = (ConnectionFlight)ob;
    return (this.origin.equals(conFlight.origin()) &&
            this.destination.equals(conFlight.destination()) &&
            this.flights.equals(conFlight.flights()));
  }
}