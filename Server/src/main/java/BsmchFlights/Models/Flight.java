package BsmchFlights.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_flights")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "id")
    private int id;

    @JsonProperty
    @Column(name = "takeofftime")
    private Date takeofftime;

    @ManyToOne
    @JsonProperty
    @JoinColumn(name = "takeofflocation")
    private Airport takeoffAirport;

    @JsonProperty
    @Column(name = "arrivaltime")
    private Date arrivaltime;

    @ManyToOne
    @JsonProperty
    @JoinColumn(name = "arrivallocation")
    private Airport arrivalAirport;

    @JsonProperty
    @Column(name = "price")
    private double price;

    @ManyToOne
    @JsonProperty
    @JoinColumn(name = "planeid")
    private Plane plane;

    @JsonProperty
    @Column(name = "refund")
    private double refund;

    @JsonProperty
    @Column(name = "refund_type")
    private String refundType;

    public Flight() {
    }

    public Flight(int id) {
        this.id = id;
    }

    public Flight(
            int id,
            Date takeoff_time,
            Airport takeoff_airport,
            Date arrival_time,
            Airport arrival_airport,
            double price,
            Plane plane,
            double refund,
            String refundType) {
        this.id = id;
        this.takeofftime = takeoff_time;
        this.takeoffAirport = takeoff_airport;
        this.arrivaltime = arrival_time;
        this.arrivalAirport = arrival_airport;
        this.price = price;
        this.plane = plane;
        this.refund = refund;
        this.refundType = refundType;
    }

    public Flight(int id, Airport takeoff_airport, Airport arrival_airport) {
        this.id = id;
        this.takeoffAirport = takeoff_airport;
        this.arrivalAirport = arrival_airport;
    }

    public Flight(
            int id,
            Airport takeoff_airport,
            Airport arrival_airport,
            Date takeofftime,
            Date arrival_time) {
        this(id, takeoff_airport, arrival_airport);
        this.takeofftime = takeofftime;
        this.arrivaltime = arrival_time;
    }

    public int id() {
        return id;
    }

    public Date takeoffTime() {
        return takeofftime;
    }

    public Airport takeoffAirport() {
        return takeoffAirport;
    }

    public Date arrivalTime() {
        return arrivaltime;
    }

    public Airport arrivalAirport() {
        return arrivalAirport;
    }

    public Plane plane() {
        return this.plane;
    }

    public double price() { return this.price; }

    @Override
    public boolean equals(Object ob) {
        return (this.id == ((Flight) ob).id());
    }
}
