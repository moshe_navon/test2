package BsmchFlights.Controllers;

import BsmchFlights.Services.AirportsService;
import BsmchFlights.Utils.QueryCondition;
import BsmchFlights.DTO.AirportDTO;
import BsmchFlights.Models.Airport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/airports")
public class AirportsController {

  private final Logger logger = LoggerFactory.getLogger(AirportsController.class);

  @Autowired private AirportsService airportsService;

  @GetMapping("")
  public List<Airport> getAll() {
    logger.info("Request: GET, Path: /airports");

    return this.airportsService.getAll();
  }

  @GetMapping("/reduced")
  public List<AirportDTO> getAirportsReduced() {
    logger.info("Request: GET, Path: /airports/reduced");

    return this.airportsService.getAirportsReduced();
  }

  @GetMapping("/{iata}")
  public Airport getByIata(@PathVariable String iata) {
    logger.info("Request: GET, Path: /airports/{iata}, iata=" + iata);

    return this.airportsService.getByIata(iata);
  }

  @PostMapping("/query")
  public List<Airport> getAirportsByBounds(@RequestBody List<QueryCondition> queryConditions) {
    logger.info("Request: POST, Path: /airports/query");

    return this.airportsService.getAirportsByBounds(queryConditions.get(0).parameters());
  }
}
