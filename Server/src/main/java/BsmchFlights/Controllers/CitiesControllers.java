package BsmchFlights.Controllers;

import BsmchFlights.Models.City;
import BsmchFlights.Services.CitiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/cities")
public class CitiesControllers {

  private final Logger logger = LoggerFactory.getLogger(CitiesControllers.class);

  @Autowired private CitiesService citiesService;

  @GetMapping("")
  public List<City> getAll() {
    logger.info("Request: GET, Path: /cities");

    return this.citiesService.getAll();
  }
}
