package BsmchFlights.Controllers;

import BsmchFlights.Models.Recommendation;
import BsmchFlights.Services.RecommendationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/recommendations")
public class RecommendationController {

    private final Logger logger = LoggerFactory.getLogger(RecommendationController.class);

    @Autowired private RecommendationService recommendationService;

    @GetMapping("")
    public List<Recommendation> getAll() {
        logger.info("Request: GET, Path: /recommendations");

        return this.recommendationService.getRecommendations();
    }

    @PostMapping("")
    public void addRecommendation(@RequestBody Recommendation recommendation){
        logger.info("Request: POST, Path: /recommendations");

        this.recommendationService.addRecommendation(recommendation);
    }
}
