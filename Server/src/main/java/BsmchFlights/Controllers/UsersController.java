package BsmchFlights.Controllers;

import BsmchFlights.Exceptions.UserNotFoundException;
import BsmchFlights.Models.User;
import BsmchFlights.Services.UsersService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UsersController {


    @Autowired
    private UsersService usersService;

    private final Logger logger = LoggerFactory.getLogger(AirportsController.class);

    @GetMapping("/")
    public ResponseEntity getAllUsers() {
        logger.info("Request: GET, Path: /users");

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/email/{email}")
    public boolean getUserByEmail(@PathVariable String email) {
        logger.info("Request: GET, Path: /email/{email}");
        return usersService.checkIfUserExistsByEmail(email);
    }

    @PatchMapping("/sendChangePasswordEmail/{email}")
    public void changePasswordByEmail(@PathVariable String email, HttpServletRequest request) throws UserNotFoundException {
        logger.info("Request: GET, Path: /sendChangePasswordEmail/{email}");

        usersService.sendChangePasswordEmail(email, request);
    }

    @PatchMapping("resetPassword/{token}/{password}")
    public boolean updatePassword(@PathVariable String token, @PathVariable String password) throws UserNotFoundException {
        return usersService.changePasswordWithToken(token, password);
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user, javax.servlet.http.HttpServletRequest req) {
        logger.info("Request: POST, Path: /users/register");

        return usersService.register(user,req);
    }



}
