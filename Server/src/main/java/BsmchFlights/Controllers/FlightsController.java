package BsmchFlights.Controllers;

import BsmchFlights.Models.ConnectionFlight;
import BsmchFlights.Models.Flight;
import BsmchFlights.Services.ConnectionsService;
import BsmchFlights.Services.FlightsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/flights")
public class FlightsController {

  private final Logger logger = LoggerFactory.getLogger(FlightsController.class);

  @Autowired private FlightsService flightsService;
  @Autowired private ConnectionsService connectionsService;

  @GetMapping("")
  public List<Flight> getAll() {
    logger.info("Request: GET, Path: /flights");

    return this.flightsService.getAll();
  }

  @GetMapping("/allfrom/{iata}/on/{on}")
  public List<Flight> getFromByName(@PathVariable String iata, @PathVariable String on)
      throws ParseException {
    logger.info("Request: GET, Path: /flights/allfrom/{iata}/on/{on}, iata=" + iata + " on=" + on);

    return this.flightsService.getFiveFirstFlightsFromIata(
        iata, new SimpleDateFormat("yyyy-MM-dd").parse(on));
  }

  @GetMapping("/allto/{name}")
  public List<Flight> getToByName(@PathVariable String name) {
    logger.info("Request: GET, Path: /flights/allto/{name}, name=" + name);

    return this.flightsService.getFlightsToCityName(name);
  }

  @GetMapping("/from/{from}/to/{to}")
  public List<ConnectionFlight> getFromTo(@PathVariable String from, @PathVariable String to) {
    logger.info("Request: GET, Path: /flights/from/{from}/to/{to} , from=" + from + " to=" + to);

    return this.flightsService.formatFlights(this.flightsService.getFlightsFromTo(from, to));
  }

  @GetMapping("/from/{from}/to/{to}/on/{on}")
  public List<ConnectionFlight> getFromToOn(
      @PathVariable String from, @PathVariable String to, @PathVariable String on)
      throws ParseException {
    logger.info(
        "Request: GET, Path: /flights/from/{from}/to/{to}/on/{on}, from="
            + from
            + " to="
            + to
            + " on="
            + on);

    return this.flightsService.formatFlights(
        this.flightsService.getFlightsFromToAt(
            from, to, new SimpleDateFormat("yyyy-MM-dd").parse(on)));
  }

  @GetMapping("/from/{from}/to/{to}/on/{on}/stops/{stops}")
  public List<ConnectionFlight> getFlights(
      @PathVariable String from,
      @PathVariable String to,
      @PathVariable String on,
      @PathVariable int stops)
      throws Exception {
    logger.info(
        "Request: GET, Path: /flights/from/{from}/to/{to}/on/{on}/stops/{stops}, from="
            + from
            + " to="
            + to
            + " on="
            + on
            + " stops="
            + stops);

    return this.connectionsService.getFlights(from, to, to, new SimpleDateFormat("yyyy-MM-dd").parse(on), stops, null);
  }

  @PutMapping("")
  public void updateFlight(@RequestBody Flight flight){
    try {
      logger.info("Request : Put, Path : flights");
    this.flightsService.updateFlight(flight);
    } catch (MailException mailException) {
      logger.error("error send mail to the user");
    } catch (Exception exception) {
      logger.error("error while changing the flight");
    }
  }
  
  @GetMapping("/topnew")
  public List<Flight> getTopNewFlights()
  {
    return this.flightsService.getTopNewFlights();
  }
}
