package BsmchFlights.Controllers;

import BsmchFlights.Models.Reservation;
import BsmchFlights.Services.ReservationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/reservations")
public class ReservationsController {

  private final Logger logger = LoggerFactory.getLogger(ReservationsController.class);

  @Autowired private ReservationsService reservationsService;

  @GetMapping("")
  public List<Reservation> getAll() {
    logger.info("Request: GET, Path: /reservations");

    return this.reservationsService.getAll();
  }

  @GetMapping("/{id}")
  public Reservation findById(@PathVariable int id) {
    logger.info("Request: GET, Path: /reservations/{id}, id=" + id);

    return this.reservationsService.findById(id);
  }
  @DeleteMapping("/{id}")
  public void deleteById(@PathVariable int id) {
    logger.info("Request: DELETE, Path: /reservations/{id}, id=" + id);
    this.reservationsService.deleteById(id);
  }


  @GetMapping("/order/{confirmation_code}")
  public List<Reservation> findByConfirmationCode(@PathVariable int confirmation_code) {
    logger.info(
        "Request: GET, Path: /reservations/order/{confirmation_code}, confirmation_code="
            + confirmation_code);

    return this.reservationsService.findByConfirmationCode(confirmation_code);
  }

  @PostMapping("/add")
  public void addRes(@RequestBody Reservation newReservation) {
    logger.info("Request: POST, Path: /reservations/add");

    this.reservationsService.add(newReservation);
  }

  @PostMapping("/addList")
  public void addResList(@RequestBody List<Reservation> newReservations) {
    logger.info("Request: POST, Path: /reservations/addList");
    newReservations.forEach(System.out::println);
    this.reservationsService.addList(newReservations);
  }

  @GetMapping("/numberOfSeatsTaken/flight/{flightId}")
  public long getNumberOfSeatsTakenForFlight(@PathVariable int flightId) {
    logger.info(
        "Request: POST, Path: /reservations/numberOfSeatsTaken/flight/{flightId}, flightId="
            + flightId);

    return this.reservationsService.getNumberOfSeatsTakenForFlight(flightId);
  }

  @GetMapping("/userId/{userId}")
  public List<Reservation> findByUserId(@PathVariable int userId) {
    logger.info("Request: POST, Path: /reservations/userId/{userId}, userId=" + userId);

    return this.reservationsService.getByUserId(userId);
  }
}
