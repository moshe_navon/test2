package BsmchFlights.Controllers;

import BsmchFlights.Models.Plane;
import BsmchFlights.Services.PlanesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/planes")
public class PlanesController {

  private final Logger logger = LoggerFactory.getLogger(PlanesController.class);

  @Autowired private PlanesService planesService;

  @GetMapping("")
  public List<Plane> getAll() {
    logger.info("Request: GET, Path: /planes");

    return this.planesService.getAll();
  }
}
