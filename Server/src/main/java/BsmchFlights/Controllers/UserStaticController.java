package BsmchFlights.Controllers;

import BsmchFlights.Exceptions.UserNotFoundException;
import BsmchFlights.Services.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@CrossOrigin
@RequestMapping("/users")
public class UserStaticController {


    private final Logger logger = LoggerFactory.getLogger(AirportsController.class);


    @Autowired
    UsersService usersService;

    @GetMapping("/resetPassword/{token}")
    public String showResetPasswordForm(@PathVariable String token) throws UserNotFoundException {
        logger.info("Request: GET , Path: users/resetPassword/{token}");
        return usersService.returnChangeForm(token);
    }

    @GetMapping("/verifyEmail/{token}")
    public String showVerifyEmailFor(@PathVariable String token) throws UserNotFoundException {
        logger.info("Request Get , Path: users/verifyEmail/{token}");
        return usersService.verifyEmail(token);
    }


}