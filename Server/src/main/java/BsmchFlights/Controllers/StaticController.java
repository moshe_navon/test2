package BsmchFlights.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin
public class StaticController {

    @RequestMapping("/")
    public String index() {
    return "index.html";
    }
}