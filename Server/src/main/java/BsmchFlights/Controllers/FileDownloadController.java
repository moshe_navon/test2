package BsmchFlights.Controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@RestController
@CrossOrigin
@RequestMapping("/download")
public class FileDownloadController {
  private final Logger logger = LoggerFactory.getLogger(FileDownloadController.class);
  private static final String EXTERNAL_FILE_PATH = "./src/main/resources/public/files";

  @RequestMapping("/files/{fileName}")
  public void downloadExtension(@PathVariable String fileName, HttpServletResponse response) {
    try {
      logger.info("trying to download file, file name: " + fileName);

      File file = new File(EXTERNAL_FILE_PATH + "extension/" + fileName);
      InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
      FileCopyUtils.copy(inputStream, response.getOutputStream());

      logger.info("success to download file, file name: " + fileName );
    } catch (Exception e) {
      logger.info("field to download file, file name: " + fileName );
    }
  }
}
